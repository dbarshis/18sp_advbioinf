# Advanced Bioinformatics SP18

This repository contains the documentation, datasets, and assignments for Dan Barshis' Advanced Bioinformatics course (Biology 795-895) at Old Dominion University, as taught in the Spring of 2018.

See the [syllabus](https://bitbucket.org/dbarshis/18sp_advbioinf/src/master/syllabus/Biol795-895_AdvBioinf_Spring_2018_Barshis.pdf) for additional information.

There is no required textbook for the class. A recommended resource is :

Haddock, S. H. D. and Dunn, C. W. (2010). Practical Computing for Biologists. 
[Sinauer Associates](http://practicalcomputing.org). Recommended.

Here are some of the appendices from the book, which summarize frequently used 
commands:
[Appendices](http://practicalcomputing.org/files/PCfB_Appendices.pdf)

## Course description

This course is designed to teach students the various steps involved in analyzing next-generation sequencing data for gene expression profiling and polymorphism identification and analyses. The class will be analyzing a full, real-life Next Generation Sequencing (NGS) dataset from start to finish. The class will follow a workshop setting with a combination of lectures, paper discussions, and instructor and student lead programming sessions.

## Learning Objectives

1. Students will become comfortable using the command line and campus cluster to perform bioinformatics analyses
2. Students will be able to conduct the follow analyses using next generation sequence data:

- De novo assembly of a transcriptome
- Taxon assignment of assembled contigs
- Annotation of assembled contigs
- Differential gene expression analyses using mRNA sequencing
- Polymorphism detection and outlier analyses of sequence data (mRNA or gDNA)

## Ground rules

- Never move things from the shared classdata directory, only copy.
- No spaces in filenames or paths
- Don't run things on the head node
- Start with interactive jobs, then move to submitted jobs
- Don't use more processors than you've requested
- Don't run with scissors

## Advice

- DuckDuckGo it
- man
- copy and paste, don't type
- tab complete
- check your end of line characters
- the command line is never wrong, it's just very very literal
- DuckDuckGo it

## SLURM script header

[SLURMheader](https://bitbucket.org/dbarshis/18sp_advbioinf/src/master/SLURMheader.txt)

## Useful commands
- sbatch #for submitting jobs
- salloc #for starting an interactive job
- squeue #for checking job status, do squeue -u yourusername to look at only your jobs
- chmod #changing permissions for scripts