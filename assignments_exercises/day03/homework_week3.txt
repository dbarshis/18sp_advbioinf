Homework Week3 (document all this on your wiki page, don't forget to pwd before each entry to remind yourself where you were working):
0.5- Finish HW/exercises from week 2 if you didn't already in class
1- cp the /cm/shared/courses/dbarshis/18AdvBioinf/assignments_exercises/day03 files to your sandbox
2- cp the renamingtable_complete.txt into your fastq folder and the /cm/shared/courses/dbarshis/18AdvBioinf/scripts/renamer_advbioinf.py script into your sandbox scripts folder and less the new script and check out the usage statement
3- run the renamer_advbioinf.py script in your fastq folder using the renamingtable_complete.txt as practice and verify the output to the screen by hand
4- Uncomment the last line of the renaming script in your scripts folder that starts with os.popen and comment out the next to last line that starts with print
5- write a sbatch script to rename all the .fastq files according to the renaming table using your renamer_advbioinf.py script
6- Make sure this is all documented on your wiki page
7- The naming convention for the files is as follows:
	SOURCEPOPULATION_SYMBIOTICSTATE_GENOTYPE_TEMPERATURE.fastq
	There are 2 sources: Virginia and Rhode Island
	There are 2 symbiotic states: Brown and White
8- Next, you're going to start the process of quality trimming and filtering all the joined, renamed .fastq files in batches, by species
9- cp the script /cm/shared/courses/dbarshis/18AdvBioinf/scripts/Trimclipfilterstatsbatch_advbioinf.py into your scripts directory
10- Less the new script and check out the usage statement
11- cp the /cm/shared/courses/dbarshis/18AdvBioinf/assignments_exercises/day03/adapterlist_advbioinf.txt into the working directory with your fastq files
12- Make a sbatch script for the Trimclipfilter... script and run it on your fastq files
13- This will take a while (like days)
14- Now might be a good time to update everything on your wiki
